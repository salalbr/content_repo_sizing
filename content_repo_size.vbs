dim oSh, oFS, oFolder, strZenPath
set oFS = WScript.CreateObject("Scripting.FileSystemObject")
set oSh = CreateObject("WScript.Shell")

strZenPath = oSh.ExpandEnvironmentStrings("%ZENWORKS_HOME%\work\content-repo")
set oFolder = oFS.GetFolder(strZenPath)

ShowFolderDetails oFolder

sub ShowFolderDetails(oF)
dim F
    'wscript.echo "---" & oF.Name & "---"
    wscript.echo oF.Name & ":#Files:" & oF.Files.Count
    wscript.echo oF.Name & ":#Folders:" & oF.Subfolders.count
    wscript.echo oF.Name & ":Size(Bytes):" & oF.Size
	wscript.echo oF.Name & ":Size(MB):" & formatNumber(oF.Size/1024/1024, 1)
    for each F in oF.Subfolders
        ShowFolderDetails(F)
    next
end sub